// use client;
import React, { FC, useState, useEffect, useCallback, MouseEvent, ChangeEvent, CSSProperties } from 'react';
import dayjs, { Dayjs } from 'dayjs';

import Panel from './Panel';
import './NextDatePickerRange.css';

type onChangeType =
  | ((value: { lower?: Dayjs; upper?: Dayjs }) => void)
  | ((value: { days?: Dayjs[] }) => void);

interface Props {
  onChange: onChangeType;
  disabledDates?: (value: Dayjs) => boolean;
  strategy?: 'disallow' | 'remove';
  onError?: (value: string) => void;
  daysOfWeek?: string[];
  namesOfMonths?: string[];
  onSelection?: () => void;
  toTheBeginning?: string;
  toTheEnd?: string;
  updateBeginning?: string;
  updateEnd?: string;
  addDay?: string;
  subtractDay?: string;
  addWeek?:string;
  subtractWeek?: string;
}


const NextDataRangePicker: FC<Props> = ({
  onChange,
  disabledDates = () => false,
  strategy = undefined,
  onError = () => { },
  daysOfWeek = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
  namesOfMonths = [
    'January', 'February', 'March', 'April',
    'May', 'June', 'July', 'August',
    'September', 'October', 'November', 'December'
  ],
  onSelection = () => { },
  toTheBeginning,
  toTheEnd,
  updateBeginning,
  updateEnd,
  addDay,
  subtractDay,
  addWeek,
  subtractWeek,

}) => {
  const [currentDate, setCurrentDate] = useState<Dayjs>(dayjs().locale('pl'));
  const [daysRows, setDaysRows] = useState<number[][]>([]);
  const [lowerDay, setLowerDay] = useState<Dayjs | null>();
  const [upperDay, setUpperDay] = useState<Dayjs | null>();
  const [preselectedUpperDay, setPreselectedUpperDay] = useState<Dayjs | null>();
  const [selectedRange, setSelectedRange] = useState<number[]>([]);
  const [selectedDays, setSelectedDays] = useState<Dayjs[]>([]);
  const weekDays = daysOfWeek || ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
  const months = [
    'January', 'February', 'March', 'April',
    'May', 'June', 'July', 'August',
    'September', 'October', 'November', 'December'
  ];



  useEffect(() => {
    setDaysRows((prevRows) => {
      const firstDayOfMonth = currentDate.startOf('month');
      const lastDayOfMonth = currentDate.endOf('month');

      let rows: number[][] = [];
      let currentWeek: number[] = [];
      let counter = 1;
      if (firstDayOfMonth.format('dddd') === 'Sunday') {
        for (let i = 0; i < 6; i++) {
          currentWeek.push(0);
        }
        currentWeek.push(counter++);
      } else {
        for (let i = 1; i <= 7; i++) {
          if (i < firstDayOfMonth.day()) {
            currentWeek.push(0);
          } else {
            currentWeek.push(counter++);
          }
        }
      }
      rows.push([...currentWeek]);

      while (counter <= lastDayOfMonth.date()) {
        currentWeek = [];

        for (let i = 1; i <= 7; i++) {
          currentWeek.push(counter++);

          if (counter > lastDayOfMonth.date()) {
            break;
          }
        }

        rows.push([...currentWeek]);
      }

      return rows;
    });
  }, [currentDate]);

  useEffect(() => {
    setSelectedRange(prevRange => []);

    for (let i = 1, max = currentDate.daysInMonth(); i <= max; i++) {
      if (checkRange(i)) {
        setSelectedRange(prev => [...prev, i]);
      }
    }

    if (strategy === 'remove') {
      onChange({ days: selectedDays });
      return;
    }

    onChange({ lower: lowerDay as Dayjs, upper: upperDay as Dayjs });
  }, [lowerDay, upperDay]);


  const isLastDayOfMonth = (date: Dayjs) => {
    const currentMonth = date.month() + 1;
    const currentYear = date.year();
    const lastDayOfMonth = dayjs(`${currentYear}-${currentMonth + 1}-01`).subtract(1, 'day');

    return date.date() === lastDayOfMonth.date();
  };

  useEffect(() => {
    if (!lowerDay) return;

    if (isLastDayOfMonth(lowerDay) || lowerDay.date() === 1) {
      toBeginning();

    };


  }, [lowerDay]);

  useEffect(() => {
    if (!upperDay) return;

    if (isLastDayOfMonth(upperDay) || upperDay.date() === 1) {
      toEnd();
    };

  }, [upperDay]);

  const decreaseMonth = useCallback(() => {
    setCurrentDate((prevDate) => prevDate.subtract(1, 'month'));
  }, []);

  const increaseMonth = useCallback(() => {
    setCurrentDate((prevDate) => prevDate.add(1, 'month'));
  }, []);
  const handleDayClick = async (e: MouseEvent<HTMLDivElement>) => {
    const target = e.target as HTMLElement;

    if (target.classList.contains('days')) {
      return;
    }

    let selectedDay: number = parseInt(target.innerText);
    let selectedDate = currentDate.clone().date(selectedDay);
    if (checkDisabled(selectedDay)) {
      return;
    }
    if (!upperDay) {
      setSelectedDays(prev => []);
    }

    if (!lowerDay) {
      setSelectedDays(prev => [])
      setLowerDay(selectedDate);
      return;
    }

    if (lowerDay && !upperDay) {

      if (selectedDate.isBefore(lowerDay)) {
        await setUpperDay(lowerDay);
        await setLowerDay(selectedDate);
        onSelection();
        return;
      }
      await setUpperDay(selectedDate);
      onSelection();
      return;
    }

    if (lowerDay && upperDay) {

      if (lowerDay.isAfter(upperDay)) {
        setLowerDay(upperDay);
      }
      setLowerDay(selectedDate);
      setUpperDay(null);
      return;
    }

    setLowerDay(null);
    setUpperDay(null);
  }

  const checkRange = (day: number) => {
    if (day === 0) {
      return false;
    }

    if (!lowerDay) {
      return false;
    }
    const selectedDate = currentDate.clone().date(day);
    const isInRange =
      (selectedDate.isAfter(lowerDay) || selectedDate.isSame(lowerDay)) &&
      (selectedDate.isBefore(upperDay) || selectedDate.isSame(upperDay)) &&
      lowerDay && upperDay;

    if (isInRange && !disabledDates(selectedDate)) {
      if (!selectedDays.find((date) => date.isSame(selectedDate))) {
        setSelectedDays((prev) => [...prev, selectedDate]);
      }
    }

    if (isInRange && disabledDates(selectedDate)) {
      if (strategy === 'remove' || !strategy) {
        return false;
      }

      if (strategy === 'disallow') {
        onError('Dissalowed date in the range')
      }
    }

    if (lowerDay && upperDay) {
      return isInRange;
    }
    return false;
  }

  const updatePreselectedUpper = (day: number) => {
    if (!lowerDay) {
      return;
    }
    const selectedDate = currentDate.clone().date(day);
    setPreselectedUpperDay(selectedDate);
  }

  const checkPreselectedRange = (day: number) => {
    if (!lowerDay || !preselectedUpperDay || upperDay) {
      return false;
    }
    const selectedDate = currentDate.clone().date(day);

    const isInRange =
      ((selectedDate.isAfter(lowerDay) || selectedDate.isSame(lowerDay)) &&
        (selectedDate.isBefore(preselectedUpperDay) || selectedDate.isSame(preselectedUpperDay))) ||

      ((selectedDate.isBefore(lowerDay) || selectedDate.isSame(lowerDay)) &&
        (selectedDate.isAfter(preselectedUpperDay) || selectedDate.isSame(preselectedUpperDay)));

    return isInRange;
  }

  const checkDisabled = (day: number) => {
    const selectedDate = currentDate.clone().date(day);
    const isDisabled = disabledDates(selectedDate);
    console.log(day, isDisabled);
    return isDisabled;
  }

  const checkBegin = (day: number): boolean => {
    if (!lowerDay) {
      return false;
    }

    const selectedDate = currentDate.clone().date(day);

    if (lowerDay && upperDay) {
      return selectedDate.isSame(lowerDay);
    }

    if (preselectedUpperDay?.isBefore(lowerDay)) {
      return selectedDate.isSame(preselectedUpperDay);
    }

    return selectedDate.isSame(lowerDay);
  }

  const checkEnd = (day: number) => {

    if (!upperDay) {
      const selectedDate = currentDate.clone().date(day);
      if (preselectedUpperDay && preselectedUpperDay.isBefore(lowerDay) && selectedDate.isSame(lowerDay)) {
        return true;
      }

      if (preselectedUpperDay && !preselectedUpperDay.isBefore(lowerDay)) {
        return selectedDate.isSame(preselectedUpperDay);
      }
      return false;
    }
    const selectedDate = currentDate.clone().date(day);
    return selectedDate.isSame(upperDay);
  }

  const englishToMonth = (month: string) => {
    if (typeof namesOfMonths === 'undefined') {
      return month;
    }
    return namesOfMonths[months.indexOf(month)]
  };

  const handleChangeMonth = (e: ChangeEvent<HTMLSelectElement>) => {
    setCurrentDate(date => date.month(months.indexOf(e.target.value)));
  }

  const handleChangeYear = (e: ChangeEvent<HTMLInputElement>) => {
    setCurrentDate(date => date.year(parseInt(e.target.value)));
  }
  const toBeginning = () => {
    if (!lowerDay || !upperDay) return;
    setCurrentDate(lowerDay.clone());
  }

  const toEnd = () => {
    if (!lowerDay || !upperDay) return;
    setCurrentDate(upperDay.clone());
  }


  const addToBeginning = (numberOfDays:number) => {
    if (!lowerDay) return;
    setLowerDay((prevLowerDay) => {
      if (!prevLowerDay) return null;
      const dayToBeAdded = prevLowerDay.clone().subtract(numberOfDays, 'day');
      setCurrentDate(dayToBeAdded);
      return dayToBeAdded;
    });
    // toBeginning();
  }



  const subtractFromBeginning = (numberOfDays: number) => {
    if (!lowerDay || lowerDay.isSame(upperDay)) return;

    setSelectedDays((prev) => prev.filter(day => !day.isSame(lowerDay)))
    onChange({ days: selectedDays });
    setLowerDay((prevLowerDay) => {
      if (!prevLowerDay) return null;
      const dayToBeAdded = prevLowerDay.clone().add(numberOfDays, 'day');
      setCurrentDate(dayToBeAdded);
      return dayToBeAdded;
    });

  }


  const addToEnd = (numberOfDays: number) => {

    if (!upperDay) return;
    setUpperDay((prevUpperDay) => {
      if (!prevUpperDay) return null;

      const dayToBeAdded = prevUpperDay.clone().add(numberOfDays, 'day');
      setCurrentDate(dayToBeAdded);
      return dayToBeAdded;
    });
  }

  const subtractFromEnd = (numberOfDays:number) => {
    if (!upperDay || upperDay.isSame(lowerDay)) return;

    setSelectedDays((prev) => prev.filter(day => !day.isSame(upperDay)))
    onChange({ days: selectedDays });

    setUpperDay((prevUpperDay) => {
      if (!prevUpperDay) return null;
      const dayToBeAdded = prevUpperDay.clone().subtract(numberOfDays, 'day');
      setCurrentDate(dayToBeAdded);
      return dayToBeAdded;
    });
  }


  return (
    <div className='next-datepicker-range'>
      {(lowerDay ?? false) && (upperDay ?? false) &&
        <Panel
          toBeginning={toBeginning}
          toEnd={toEnd}
          addToBeginning={addToBeginning}
          subtractFromBeginning={subtractFromBeginning}
          addToEnd={addToEnd}
          subtractFromEnd={subtractFromEnd}
          toTheBeginning={toTheBeginning}
          toTheEnd={toTheEnd}
          updateBeginning={updateBeginning}
          updateEnd={updateEnd}
          addDay={addDay}
          subtractDay={  subtractDay }
          addWeek={ addWeek }
          subtractWeek={ subtractWeek }
        />
      }
      <div className='calendar-container-datepicker'>
        <h4 className='header-datepicker'>

          <span className="arrow-datepicker left-datepicker" onClick={decreaseMonth}></span>
          <div className="year-and-month-wrapper-datepicker">
            <input
              className="yearinput-datepicker"
              min={currentDate.clone().subtract(100, 'year').format('YYYY')}
              max={currentDate.clone().add(100, 'year').format('YYYY')}
              step={1}
              placeholder={currentDate.year() + ''}
              size={4}
              value={currentDate.format('YYYY')}
              onChange={handleChangeYear}
              type='number' />

            <select className="select-datepicker" onChange={handleChangeMonth}
              value={currentDate.format('MMMM')}
            >
              {
                months.map((month, index) => {
                  return <option value={month} key={index}>{englishToMonth(month)}</option>
                })
              }
            </select>
          </div>
          <span className="arrow-datepicker" onClick={increaseMonth}></span>
        </h4>
        <div className='weekline-datepicker'>
          {weekDays.map((item, index) => (
            <div
              key={index}
              className={`${index === 6 ? 'sunday-container-datapicker' : ''} weekname-datepicker`}>
              {item}
            </div>
          ))}
        </div>
        <div>
          {daysRows.map((week, weekIndex) => (
            <div key={weekIndex}
              className="days weekline-datepicker"
              onClick={(e) => { handleDayClick(e) }}
            >
              {week.map((day, dayIndex) => (
                <div key={dayIndex}
                  className={`ceil-datepicker ${dayIndex === 6 ? 'sunday-container-datapicker' : ''} ${checkPreselectedRange(day) && !checkDisabled(day) ? 'preselected-datapicker' : ''} ${checkRange(day) ? 'selected-datepicker' : ''} ${checkBegin(day) ? 'begin' : ''} ${checkEnd(day) ? 'end' : ''}`}
                  onMouseEnter={() => { updatePreselectedUpper(day) }}>
                  <div
                    className={`${checkDisabled(day) ? 'disabled-datapicker' : ''}`}
                  >
                    {day > 0 ? day : ''}
                  </div>
                </div>
              ))}
            </div>
          ))}
        </div>
      </div>

    </div>
  );
};

export default NextDataRangePicker;
