import React, { FC } from 'react';

interface Props {
    toBeginning: () => void;
    toEnd: () => void;
    addToBeginning: (numberOfDays:number) => void;
    subtractFromBeginning: (numberOfDays:number) => void;
    addToEnd: (numberOfDays:number) => void;
    subtractFromEnd: (numberOfDays:number) => void;
    toTheBeginning?: string;
    toTheEnd?: string;
    updateBeginning?: string;
    updateEnd?: string;
    addDay?: string;
    subtractDay?: string;
    addWeek?: string;
    subtractWeek?: string;
}

const Panel: FC<Props> = ({
    toBeginning,
    toEnd,
    addToBeginning,
    subtractFromBeginning,
    addToEnd,
    subtractFromEnd,
    toTheBeginning,
    toTheEnd,
    updateBeginning,
    updateEnd,
    addDay,
    subtractDay,
    addWeek,
    subtractWeek
}: Props) => {
    return (
        <div className='buttons-wrapper-datepicker'>
            <button onClick={toBeginning} className='button-datepicker' style={{ marginBottom: '2px' }}>
                { toTheBeginning || `To the beginning` }
            </button>
            <button onClick={toEnd} className='button-datepicker'>
                { toTheEnd || `To the end` }
            </button>
            { updateBeginning || `Update beginning` }
            <div className="vertically">
                <button onClick={()=>{addToBeginning(1)}} className='button-datepicker'>
                    { addDay || `Add day` }
                </button>
                <button onClick={()=>{subtractFromBeginning(1)}} className='button-datepicker'>
                    { subtractDay || `Subtract day`}
                </button>
            </div>
            
            <div className="vertically">
                <button onClick={()=>{addToBeginning(7)}} className='button-datepicker'>
                    { addWeek || `Add week` }
                </button>
                <button onClick={()=>{subtractFromBeginning(7)}} className='button-datepicker'>
                    { subtractWeek || `Subtract week`}
                </button>
            </div>
            { updateEnd || `Update end` }
            <div className="vertically">
                <button onClick={()=>{addToEnd(1)}} className='button-datepicker'>
                    { addDay || `Add day`}
                </button>
                <button onClick={()=>{subtractFromEnd(1)}} className='button-datepicker'>
                    { subtractDay || `Substract day`}
                </button>
            </div>
            <div className="vertically">
                <button onClick={()=>{addToEnd(7)}} className='button-datepicker'>
                    { addWeek || `Add week`}
                </button>
                <button onClick={()=>{subtractFromEnd(7)}} className='button-datepicker'>
                    { subtractWeek || `Substract week`}
                </button>
            </div>
            
        </div>
    );
};

export default Panel;
