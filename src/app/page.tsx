'use client';
import { useState } from 'react';
import { Dayjs } from 'dayjs';
import styles from './page.module.css';
import NextDatePickerRange from './components/NextDatePickerRange';

export default function Home() {
  const [lowerDay, setLowerDay] = useState<Dayjs | null | undefined>(null);
  const [upperDay, setUpperDay] = useState<Dayjs | null | undefined>(null);
  const [days, setDays] = useState<Dayjs[]>([]);
  const [calendarOpened, setCalendarOpened] = useState(false);

  const disabledDates = (data: Dayjs): boolean => {
    return data.day() === 0 || data.day() === 3;
  };

  return (
    <main className={styles.main}>
      <h4 className='description'>Simple DatePickerRange</h4>
      <br />
      {typeof lowerDay !== 'undefined' && `${lowerDay?.year()}/ ${lowerDay?.month()} /${lowerDay?.date()}`} -
      {typeof upperDay !== 'undefined' && `${upperDay?.year()}/ ${upperDay?.month()} /${upperDay?.date()}`}
      <br /><br />
      <NextDatePickerRange
        onChange={({ lower, upper }: { lower?: Dayjs, upper?: Dayjs }) => {
          setLowerDay(lower);
          setUpperDay(upper);
        }}
      />
      <br />
      <h4 className='description'>Disabled dates in DatePickerRange</h4>
      <br />
      {days.map((day, index) => {
        return <p key={index}>{`${day?.year()}/ ${day?.month()} /${day?.date()}`}</p>;
      })}
      <br /><br />
      <NextDatePickerRange
        onChange={(value: { days?: Dayjs[] }) => {
          setDays(value.days || []);
        }}
        disabledDates={disabledDates}
        strategy="remove"
      />
      <br />
      <h4 className='description'>Simple Date Picker Range with Custom Day and Month Names</h4>
      <br /><br />
      <NextDatePickerRange
        onChange={({ lower, upper }: { lower?: Dayjs, upper?: Dayjs }) => {
          setLowerDay(lower);
          setUpperDay(upper);
        }}
        daysOfWeek={['Pon', 'Wt', 'Śr', 'Czw', 'Pią', 'Sob', 'Nie']}
        namesOfMonths={[
          'Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec',
          'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'
        ]}
      />
      <br />
      <h4 className='description'>Close calendar on selection</h4>
      <br />
      <br />
      <button className='material-button' onClick={() => { setCalendarOpened(value => !value) }}>Open / Close calendar </button>
      {typeof lowerDay !== 'undefined' && `${lowerDay?.year()}/ ${lowerDay?.month()} /${lowerDay?.date()}`} -
      {typeof upperDay !== 'undefined' && `${upperDay?.year()}/ ${upperDay?.month()} /${upperDay?.date()}`}
      {calendarOpened && <NextDatePickerRange
        onChange={({ lower, upper }: { lower?: Dayjs, upper?: Dayjs }) => {
          setLowerDay(lower);
          setUpperDay(upper?.clone());
        }}
        daysOfWeek={['Pon', 'Wt', 'Śr', 'Czw', 'Pią', 'Sob', 'Nie']}
        namesOfMonths={[
          'Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec',
          'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'
        ]}
        onSelection={() => { setCalendarOpened(false) }}
      />}
      <br />
      <h4 className='description'>Full translation</h4>
      <br />
      <br />

      <NextDatePickerRange
        onChange={({ lower, upper }: { lower?: Dayjs, upper?: Dayjs }) => {
          setLowerDay(lower);
          setUpperDay(upper);
        }}
        daysOfWeek={['Pon', 'Wt', 'Śr', 'Czw', 'Pią', 'Sob', 'Nie']}
        namesOfMonths={[
          'Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec',
          'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'
        ]}
        
        toTheBeginning="Do początku"
        toTheEnd="Do końca"
        updateBeginning="Zmień początek"
        updateEnd="Zmień koniec"
        addDay="Dodaj dzień"
        subtractDay="Odejmin dzień"
        addWeek='Dodaj tydzień'
        subtractWeek='Odejmin tydzień'

      />


    </main>
  );
}
